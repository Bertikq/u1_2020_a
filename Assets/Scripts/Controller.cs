﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public Transform head;
    public int speed;
    public float sensitivity;
    public int forceJump;
    public int hp;
    public Text textHp;

    private float rotateY;

    private void Update()
    {

        if (hp <= 0)
        {
            GetComponent<ShootController>().weapon.GetComponent<Rigidbody>().isKinematic = false;
            GetComponent<Animator>().Play("DeadAnimation");
        }
        else
        {
            textHp.text = "HP: " + hp;
            float vertical = Input.GetAxis("Vertical");
            float horizontal = Input.GetAxis("Horizontal");

            float vMouse = Input.GetAxis("Mouse Y");
            float hMouse = Input.GetAxis("Mouse X");

            float rotateX = head.localEulerAngles.y + hMouse * sensitivity;
            rotateY = rotateY - vMouse * sensitivity;
            head.localEulerAngles = new Vector3(rotateY, rotateX, 0);

            Vector3 direction = new Vector3(horizontal, 0, vertical);
            direction = head.TransformDirection(direction);
            direction = new Vector3(direction.x, 0, direction.z);
            transform.position += direction * Time.deltaTime * speed;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                gameObject.GetComponent<Rigidbody>().AddForce(Vector3.up * forceJump);
            }

            if (Input.GetKeyDown(KeyCode.T))
            {
                Vector3 superJump = new Vector3(head.forward.x, 1, head.forward.z);
                gameObject.GetComponent<Rigidbody>().AddForce(superJump * 100);
            }

            if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
        }
    }
}
