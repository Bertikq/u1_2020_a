﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootController : MonoBehaviour
{
    public Transform weapon;
    public GameObject bulletPrefab;
    public Text textInstr;
    public Transform weaponPosition;
    public Text textAmmo;
    public Text textAllAmmo;
    public int countAmmo;
    public Scrollbar scrollbar;
    public float timeLeftReload;

    public AudioSource audioSourceShot;

    public void getAmmo(int ammo)
    {
        countAmmo += ammo;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "AmmoBox")
        {
            getAmmo(5);
            Destroy(other.gameObject);
        }
    }

    private void Update()
    {
        textAllAmmo.text = "Патронов: " + countAmmo;
        textAmmo.text = weapon.GetComponent<SettingWeapon>().ammo.ToString();
        if (timeLeftReload > 0)
        {
            timeLeftReload -= Time.deltaTime;
            scrollbar.gameObject.SetActive(true);
        }
        else
        {
            scrollbar.gameObject.SetActive(false);
        }
        scrollbar.size = timeLeftReload / 
            weapon.GetComponent<SettingWeapon>().timeReload;
        if (timeLeftReload < 0.1 && Input.GetKeyDown(KeyCode.R))
        {
            SettingWeapon curWeapon = weapon.GetComponent<SettingWeapon>();
            if (countAmmo >= curWeapon.maxSizeAmmo)
            {
                timeLeftReload = curWeapon.timeReload;
                countAmmo -= curWeapon.maxSizeAmmo;
                curWeapon.ammo = curWeapon.maxSizeAmmo;
            }
            else
            {
                timeLeftReload = curWeapon.timeReload;
                curWeapon.ammo += countAmmo;
                countAmmo = 0;
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            if (weapon.GetComponent<SettingWeapon>().ammo > 0)
            {
                weapon.GetComponent<SettingWeapon>().ammo--;
                GameObject bullet = Instantiate(bulletPrefab);
                bullet.transform.position = weapon.GetComponent<SettingWeapon>().endWeapon.position;
                bullet.GetComponent<Rigidbody>().AddForce(-weapon.forward * weapon.GetComponent<SettingWeapon>().forceShot);
                audioSourceShot.Play();
            }
        }

        Vector3 centerScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        Ray ray = Camera.main.ScreenPointToRay(centerScreen);
        RaycastHit raycastHit;

        if (Physics.Raycast(ray, out raycastHit))
        {
            if (raycastHit.transform.CompareTag("Weapon"))
            {
                textInstr.gameObject.SetActive(true);
                textInstr.text = "Чтобы подобрать " + raycastHit.transform.
                    GetComponent<SettingWeapon>().nameWeapon + "\n Нажмите F";

                if (Input.GetKeyDown(KeyCode.F))
                {
                    raycastHit.transform.parent = weapon.parent;
                    raycastHit.transform.position = weaponPosition.position;
                    raycastHit.transform.GetComponent<BoxCollider>().enabled = false;
                    raycastHit.transform.GetComponent<Rigidbody>().isKinematic = true;
                    raycastHit.transform.localRotation = Quaternion.Euler(0, 180, 0);
                    weapon.parent = null;
                    weapon.GetComponent<Rigidbody>().isKinematic = false;
                    weapon.GetComponent<BoxCollider>().enabled = true;
                    weapon = raycastHit.transform;
                }
            }
            else
            {
                textInstr.gameObject.SetActive(false);
            }
        }
        else
        {
            textInstr.gameObject.SetActive(false);
        }
    }
}
