﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Finish : MonoBehaviour
{
    public Text text;
    public BallController ballController;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball" && ballController.countTeapots > 2)
        {
            SceneManager.LoadScene("Menu");
        }
    }

}
