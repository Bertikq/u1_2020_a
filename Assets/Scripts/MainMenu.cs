﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject panelMain;
    public GameObject panelLevels;

    public void ButtonPlay()
    {
        panelMain.SetActive(false);
        panelLevels.SetActive(true);
    }

    public void ButtonToMenu()
    {
        panelLevels.SetActive(false);
        panelMain.SetActive(true);
    }
}
